var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'), //sass sourcemaps
    sass = require('gulp-sass'), //compile sass
    autoprefixer = require('gulp-autoprefixer'), //vendor prefixes
    uglify = require('gulp-uglify'), //minify js
    jshint = require('gulp-jshint'), //jshint
    concat = require('gulp-concat'), //concatenate files
    imagemin  = require('gulp-imagemin'), //optimize images
    browsersync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    beeper = require('beeper');



function onError(err) {
  beeper();
  console.log(err);
}
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 1%', 'Firefox ESR']
};

//do sass related stuff
gulp.task('sass', function() {
  return gulp.src('app/sass/*.scss')
    .pipe(plumber({
      errorHandler: onError    
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerOptions)) 
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('app/build/css'))
});

//jshint as a separate tasks when I want it
gulp.task('jshint', function () {
  return gulp.src(['app/js/badGuys.js', 'app/js/app.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
});
//image minification as a separate tasks when I need it
gulp.task('images', function() {
  return gulp.src('app/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('app/build/images'))
});

//js related tasks
gulp.task('js', function () {
  return gulp.src(['app/js/vendors/firebase-2.4.0.js','app/js/vendors/fastclick.js', 'app/js/vendors/jquery-2.1.4.js', 'app/js/vendors/handlebars-v4.0.5.js', 'app/js/vendors/velocity.min.js', 'app/js/vendors/velocity.ui.min.js', 'app/js/badGuys.js', 'app/js/app.js', 'app/js/leader-board.js'])
    .pipe(plumber({
      errorHandler: onError    
    }))
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/build/js'));
});


//run of the mill node server
// gulp.task('server', function() {
//   return connect().use(serve(__dirname))
//     .listen(8080)
//     .on('listening', function() {
//       console.log('Server is running: view at http://localhost:8080');
//     });
// });

//browsersync
gulp.task('browsersync', function(cb) {
    return browsersync({
      server: {
        baseDir: './app'
      }
    }, cb);
});
//watch for changes - run tasks - reload browsers
gulp.task('watch', function() {
    gulp.watch('app/sass/*.scss', ['sass', browsersync.reload]);
    gulp.watch('app/js/*.js', ['js', browsersync.reload]);
});
//default task
gulp.task('default', ['sass', 'js', 'browsersync', 'watch']);


//run 'gulp' to spin up server and compile sass and ctrl+c to stop servergit 

// point url to http://localhost:8080/app/#/standings

//need: livereload, browsersync, css minify, js minify

//articles: 
//http://code.tutsplus.com/tutorials/managing-your-build-tasks-with-gulpjs--net-36910
//http://code.tutsplus.com/tutorials/gulp-as-a-development-web-server--cms-20903

//note - i may need a package.json file 

//sass, autoprefixer, server, watch

//.task(), .src(), .watch(), .dest(), .series(), .parallel() - core gulp methods
//.pipe() - node.js method

// .task('name-of-task', callback function)
//basic wrapper for creating our tasks - name of task and a callback function that contains code to execute the function

//.src() - location of source files
//.src(string || array of strings)
//.src('css/*.scss')
//.src(['css/*.scss', 'less/*.less'])

//.watch() look for changes in our files
//.watch(string, array)

//.dest('location-of-destination') set the output destination of your processed file


//.pipe() - pipe together smaller single-purpose plugins into a pipechain. controls order. 
//.pipe(function)

//NOTE: .series() and .parallel are not available until version 4.0 - you have 3.?
