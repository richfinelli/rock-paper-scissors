var badGuys = { 
	badGuys: [
		{
			id: 0,
			shortCode: "oh",
			name: "Oh",
			slogan: "Hello!",
			bio: "An alien on the run from his own people makes friends with a girl. He tries to help her on her quest, but ends up as a laughable, but lovable interference.",
			tendencies: "Throws rock often, but never more than twice in a row.",
			skill: "Easy",
			points: 100,
			defeated: null
		},
		{
			id: 1,
			shortCode: "ned-stark",
			name: "Ned Stark",
			slogan: "Winter is coming", 
			bio: "Eddard Stark, Lord of Winterfell, Warden of the North, honorable and loyal to a fault. Fostered in the Vale by Jon Arryn, became close friends with Robert Baratheon. Later Ned and Robert rise in revolt after King Aerys executes Ned's father.",
			tendencies: "Relies heavily on throwing paper.",
			skill: "Medium",
			points: 250,
			defeated: null
		},
		{
			id: 2,
			shortCode: "stone-cold",
			name: "Stone Cold",
			slogan: "Cuz Stone Cold said so!",
			bio: "Former WWE Champion, loves Budweiser and giving a good whoop ass. Defies all authority, especially Vince McMahon. Starred in the television show Nash Bridges from 1998-1999.",
			tendencies: "Throws scissors quite a bit when not behind. Cheats when trailing.",
			skill: "Tricky",
			points: 500,
			defeated: null
		},
		{
			id: 3,
			shortCode: "darth-maul",
			name: "Darth Maul",
			slogan: "You will pay for your insolence.",
			bio: "Sith Lord who lived during the waning years of the Galactic Republic. Apprentice to Darth Sidious.",
			tendencies: "No tendencies, uses the power of the darkside when behind.",
			skill: "Difficult",
			points: 1000,
			defeated: null
		},
		{
			id: 4,
			shortCode: "grumpy-cat",
			name: "Grumpy Cat",
			slogan: "Meow!",
			bio: "Grumpy, annoyed, precious.",
			tendencies: "Completely random.",
			skill: "Impossible",
			points: 2500,
			defeated: null
		}
	]
};



