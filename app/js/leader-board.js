
	// Create our Firebase reference
	//reference to the firebase database - user object
	var ref = new Firebase('https://rpsbattle.firebaseio.com//users');

  // Helper function that takes a new score snapshot and adds an appropriate row to our leaderboard table.
  
    // var newScoreRow = $("<div class='leader-board-container' />");
    // newScoreRow.append($("<div class='leader-row'/>").append($("<div class='leader-name'/>").text(scoreSnapshot.val().name)).append($("<div class='leader-score'/>").text(scoreSnapshot.val().score)));
if (pageLeaderBoard) {
  ref.orderByChild("Score").limitToFirst(100).on("child_added", function(snapshot, prevChildKey) {
   $(".leader-totals").after(
      "<div class='leader-row'>" + 
        "<div class='leader-place'></div>" +
        "<div class='leader-name'>" + snapshot.val().Name + "</div>" +
        "<div class='leader-score'>" + snapshot.val().Score + "</div>" +
      "</div>"
    );
  });
  ref.once("value", function(snapshot) {
    $(".leader-row").not(".leader-totals").each(function(i, val) {
      $(this).children(".leader-place").text(i + 1);
    });
  });
}









