var rps = {
	yourName: null,
	yourChoice: null,
	yourChoiceHistory: null,
	yourPoints: 0,
	yourOverallScore: 0,
	opponentsName: null,
	opponentsShortCode: null,
	opponentsId: null,
	opponentsChoice: null,
	opponentsChoiceHistory: null,
	opponentsPoints: 0,
	roundwinner: null,
	overallWinner: null,
	playTo: 1,
	pointsMultiplier: 1
};
var baddies = badGuys.badGuys;
var rpsStorage = localStorage;
var pageLeaderBoard = $("body").hasClass("page-leader-board");
var pageGame = $("body").hasClass("page-game");


//save your name to local storage
function saveNameToLocalStorage() {
	if (!rpsStorage.getItem("yourName")) {
		//if localStorage doesn't know your name, add your name (rps.yourName) to localStorage
		rpsStorage.setItem("yourName", rps.yourName);
	} else {
		//if localStorage does know your name, update rps.yourName with the value in localStorage
		rps.yourName = rpsStorage.getItem("yourName");
	}
}
// function savePlayToToLocalStorage() {
// 	if (!rpsStorage.getItem("playTo")) {
// 		//if localStorage doesn't know playTo #
// 		rpsStorage.setItem("playTo", rps.playTo);
// 	} else {
// 		//if localStorage does know your playTo, then update the object
// 		rps.playTo = Number(rpsStorage.getItem("playTo"));
// 	}
// }
//to do list:
//1 - optimize images better - a) use srcset for images, or a sprite
//2 - make closer to AAA ADA compliant
//3 - run through Google page speed index - get 100/100 for desktop and mobile

function saveDefeatedEnemiesToLocalStorage() {
	if (!rpsStorage.getItem("defeatedEnemies")) {
	} else {
		//function that (A) updates teh baddies object and (B) updates the DOM
		function markBaddyAsDefeated(baddyId) {
			baddies[baddyId].defeated = true; // update the baddies object with the defeated enemy
			//mark the baddy as defeated (in DOM)
			$(".choose-opponent figure").eq(baddyId).addClass("defeated")
				.attr("disabled", "disabled")
				.find(".opponent-indication").html("&#10006;");
		}
		//if all enemies are defeated, reset local storage (but not yourName)
		if (rpsStorage.getItem("defeatedEnemy4")) {
			rpsStorage.removeItem("defeatedEnemy0");
			rpsStorage.removeItem("defeatedEnemy1");
			rpsStorage.removeItem("defeatedEnemy2");
			rpsStorage.removeItem("defeatedEnemy3");
			rpsStorage.removeItem("defeatedEnemy4");
		} else {
			if (rpsStorage.getItem("defeatedEnemy0")) {
				markBaddyAsDefeated(0);
			}
			if (rpsStorage.getItem("defeatedEnemy1")) {
				markBaddyAsDefeated(1);
			}
			if (rpsStorage.getItem("defeatedEnemy2")) {
				markBaddyAsDefeated(2);
			}
			if (rpsStorage.getItem("defeatedEnemy3")) {
				markBaddyAsDefeated(3);
				introduceFinalBoss();
			}
		}

	}
}

//establish points multiplier (more points awarded for best of 5, even more for best of 7)
function multiplierPoints(playTo) {
	var factor = playTo * playTo / 5;
	if (factor < 1) {
		return rps.pointsMultiplier = 1;
	} else {
		return rps.pointsMultiplier = factor;
	}
}

function proceedToOpponentSelection() {
	rps.yourName = $("#your-name").val();
	rps.playTo = Number($(".rps-play-to:checked").val());

	saveNameToLocalStorage();
	//savePlayToToLocalStorage(); //no longer need this function, just need the below statement to do the job.
	//if localStorage doesn't know playTo #
	rpsStorage.setItem("playTo", rps.playTo);

	//this makes sure if you are playing to 5 you get more points for winning than playing to 3 (and even more if playing to 7)
	multiplierPoints(rps.playTo);

	$(".getting-started").addClass("hide");
	$.get("./partials/choose-opponent.html", function(data) {
		$(".choose-opponent").html(data);

		//make up a name if the player doesn't provide it
		if (rps.yourName === null || rps.yourName == "") {
			rps.yourName = "John Wayne";
		}
		$(".your-name-display").text(rps.yourName);

		//handlebars template
		var template = $("#opponents-template").html();
		var compiledTemplate = Handlebars.compile(template); 
		$(".opponents-selection-container").html(compiledTemplate(badGuys));

		//hide the final boss
		$(".opponent-grumpy-cat").addClass("hide");

		saveDefeatedEnemiesToLocalStorage();
	});
}
function selectOpponent(e) {
	if ( $(e.target).is(".open-opponent-bio") ) {
		e.stopPropagation();
	} else {
		$(".opponents-selection-container").find("figure").not(this).removeClass("active");
		//don't allow the selection of defeated opponents
		if (!$(this).hasClass("defeated")) {
			$(this).addClass("active");
			$(".confirm-opponent").removeClass("disabled").removeAttr("disabled");
		}
	}
}
function bestOfCalc() {
	if (rps.playTo === 2) {
		return 3;
	} else if (rps.playTo === 3) {
		return 5;
	} else {
		return 7;
	}
}
function proceedToRPS() {
	$("body").removeClass("isolation-mode");
	$(".top-scores").addClass("hide");
	$(".score-card").removeClass("hide");
	$(".play-to-display").text(bestOfCalc());
	$(".best-of").removeClass("hide");
	var badGuyID = $("figure.active").data("opponent-id");
	rps.opponentsName = baddies[badGuyID].name;
	rps.opponentsId = baddies[badGuyID].id;
	rps.opponentsShortCode = "images/" + baddies[badGuyID].shortCode + ".jpg";
	$(".choose-opponent").addClass("hide");
	$.get("./partials/play-game.html", function(data) {
		$(".play-game").html(data).removeClass("hide");
		$(".your-name-display").text(rps.yourName);
		$(".opponents-name-display").text(rps.opponentsName);
	});
	closeOpponentBio();
	if (rps.opponentsId === 4) {
		$("body").removeClass("isolation-mode");
	}
}
//determines rock, paper, or scissors probability
function rpsGenerator(rockNum, paperNum) {	
	var randomNumber = Math.floor(Math.random() * 9) //produces a random # btwn 0 and 8 
	if(randomNumber <= rockNum) {
		rps.opponentsChoice = "rock";
	} else if (randomNumber > rockNum && randomNumber <= paperNum) {
		rps.opponentsChoice = "paper";
	}
	else {
		rps.opponentsChoice = "scissors";
	}
}
var ohCount = 0;
//"randomly" chooses the opponents choice
function opponentsChoice() {
	
	//rpsGenerator(9,0);

	//Ned Stark
	//Honorable, aged
	//He tends to throw paper more
	//doesn't cheat
	if (rps.opponentsName === "Ned Stark") {
		rpsGenerator(1, 7);
	}
	
	//Oh
	//Cute, predictable
	//throws rock more often, but never more than twice in a row
	else if (rps.opponentsName === "Oh") {
		if (ohCount < 2) {
			rpsGenerator(5, 7);//high probability of rock
			ohCount = ohCount + 1;
		} else {
			rpsGenerator(0,4); //super low probability of rock, equal probability paper or scissors
			ohCount = 0;
		}
	}

	//Stone Cold
	//Badass, cocky, cheats when behind
	//throws scissors more often when a tie or ahead, cheats when behind
	else if (rps.opponentsName === "Stone Cold") {
		//if stone cold is behind he starts cheating
		if (rps.yourPoints > rps.opponentsPoints) {
			//cheat logic here.
			if (rps.yourChoice === "rock") {
				rpsGenerator(0,7); //high probability for paper
			} else if (rps.yourChoice === "paper") {
				rpsGenerator(0,2); //high probability for scissors
			} else {
				rpsGenerator(7,8); //high probability for rock
			}
		} else {
			rpsGenerator(1,3); //high probability of scissors
		}
	}
	//Darth Maul
	//use power of the dark-side when behind
	//
	else if (rps.opponentsName === "Darth Maul") {
		if (rps.yourPoints > rps.opponentsPoints) {
			if (rps.yourChoice === "rock") {
				rpsGenerator(1,6); //high probability for paper
			} else if (rps.yourChoice === "paper") {
				rpsGenerator(1,3); //high probability for scissors
			} else {
				rpsGenerator(6,7); //high probability for rock
			}
		} else {
			if (rps.yourChoice === "rock") {
				rpsGenerator(2,5); //random
			} else if (rps.yourChoice === "paper") {
				rpsGenerator(1,3); //high probability for scissors
			} else {
				rpsGenerator(2,5); //random
			}
		}
		//rpsGenerator(9,0);
	}
	// //Grumpy cat
	// //Throws constant hay makers
	else if (rps.opponentsName === "Grumpy Cat") {	
			rpsGenerator(2,5);
	}	
	// just in case (but shouldn't be used unless a new opponent is switched in)
	else { 
		rpsGenerator(2,5);
	}
}
//update Round Score
function updateRoundScore() {
	if (rps.roundWinner === rps.yourName) {
		rps.yourPoints = rps.yourPoints + 1;
	} else if (rps.roundWinner === rps.opponentsName) {
		rps.opponentsPoints = rps.opponentsPoints + 1;
	}
	$(".your-score").text(rps.yourPoints);
	$(".opponents-score").text(rps.opponentsPoints);
}
function yourOverallScoreUpdate() {
	if (rps.overallWinner === rps.yourName) {
		return baddies[rps.opponentsId].points * rps.pointsMultiplier; //you earn opponents skill level * pointsMultiplier
	} else {		
		return baddies[rps.opponentsId].points / -4; // deduct 1/4 opponents skill level
	}
}
function determineOverallWinner() {
	if (rps.yourPoints === rps.playTo) {
		rps.overallWinner = rps.yourName;
	} else {
		rps.overallWinner = rps.opponentsName;
	}
}
//display overall winner 
function displayOverallWinner() {
	$(".play-game").addClass("hide");
	$.get("./partials/the-winner.html", function(data) {
		$(".the-winner").html(data).removeClass("hide");
		
		//you win
		if (rps.overallWinner === rps.yourName) {
			$(".the-winner h1").text("You won, " + rps.yourName + "!");
			$(".the-winner .experience-points").text("You earned +" + yourOverallScoreUpdate() + "EP!");
			
			$(".the-winner .experience-points").velocity("callout.pulse", {delay: 1000});
			if (rps.opponentsId === 4) { 
				//final boss defeated
				$(".play-again").text("Roll credits");
			} else { 
				//any other baddy defeated
				$(".play-again").text("Next opponent");
			}

			
		} 
		//you lose
		else if (rps.overallWinner === rps.opponentsName) {
			$(".the-winner img").attr({
				"src": rps.opponentsShortCode,
				"alt": rps.opponentsName
			});
			$(".the-winner h1").text(rps.opponentsName + " defeated you.");	
			if (rps.yourOverallScore <= 0) {
				$(".the-winner .experience-points").text("Keep trying to earn experience points.");
			} else {
				$(".the-winner .experience-points").text("You lost " + yourOverallScoreUpdate() + "EP.");
				$(".the-winner .experience-points").velocity("callout.tada", {delay: 1000});
			}
			$(".play-again").text("Try again");
		}
		//add new EP earned to total experience points (EP)
		rps.yourOverallScore = rps.yourOverallScore + yourOverallScoreUpdate();
		//don't let total EP dip below 0.
		if (rps.yourOverallScore < 0) {
			rps.yourOverallScore = 0;
		}
		//update local storage with your score
		rpsStorage.setItem("yourScore", rps.yourOverallScore);
		//display total experience points
		$(".your-experience-container .experience-points").text(rps.yourOverallScore);
		$(".your-experience-container").removeClass("hide");
	});
}
//determine who won
function determineResults() {
	if(rps.opponentsChoice !== null && rps.yourChoice !== null) {
		if (rps.opponentsChoice === "rock") {
			if (rps.yourChoice === "rock") {
				rps.roundWinner = "tie";
			} else if (rps.yourChoice === "scissors") {
				rps.roundWinner = rps.opponentsName;
			} else { //you choose paper
				rps.roundWinner = rps.yourName;
			}
		} else if (rps.opponentsChoice === "scissors") {
			if (rps.yourChoice === "rock") {
				rps.roundWinner = rps.yourName;
			} else if (rps.yourChoice === "scissors") {
				rps.roundWinner = "tie";
			} else { //you choose paper
				rps.roundWinner = rps.opponentsName;
			}
		} else { //opponent chooses paper
			if (rps.yourChoice === "rock") {
				rps.roundWinner = rps.opponentsName;
			} else if (rps.yourChoice === "scissors") {
				rps.roundWinner = rps.yourName;
			} else { //you choose paper
				rps.roundWinner = "tie";
			}
		}
	}
	//Update Round Score
	updateRoundScore();

	//if the match has been decided
	if (rps.yourPoints === rps.playTo || rps.opponentsPoints === rps.playTo) {
		determineOverallWinner();
		displayOverallWinner();
	} 
}
function announceRoundResults() {
	var announceWinner = "";
	if (rps.roundWinner === "tie") {
		announceWinner = "It's a tie!";
	} else if (rps.roundWinner === rps.opponentsName) {
		announceWinner = rps.roundWinner + " won this round!";
	} else {
		announceWinner = "You won this round!";
	}
	$(".winner-announcement").removeClass("hide").children("h2").html(announceWinner);
	$(".instructions-text").html("Go again, <br>")
}
function disableMakingASelection() {
	//$(".choice-container .rps-radio").prop("checked", false);
	$(".choice-container label").addClass("disabled");
	$(".rps-radio, .play-rps").attr("disabled", "disabled");
	$(".play-rps").addClass("disabled");
}
function prepForNextRound() {
	$(".play-rps").removeClass("disabled").removeAttr("disabled"); //enable the Choose button
	$(".rps-radio, .play-rps").removeAttr("disabled"); //enable radio buttons
	$(".play-game label").removeClass("opponents-choice-active"); //de-activate opponents choice
	$(".rps-radio").prop("checked", false); //uncheck your choice radio button
}
function playRPS() {
	var buttonIsDisabled = $(this).hasClass("disabled");
	var nothingIsChosen = !$(".rps-radio:checked").length;
	if (buttonIsDisabled) {
		return false;
	} 
	else if(nothingIsChosen) {
		$(".choice-container").velocity("callout.shake");
		return false;

	} else {
		disableMakingASelection();
		var yourSelection = $(".rps-radio:checked").val();
		rps.yourChoice = yourSelection;
		$(".winner-announcement").addClass("hide");
		$(".instructions-container").velocity("fadeOut", {
			complete: function() {
				$(".opponents-choice").html(rps.opponentsName + " is choosing...");
				$(".loading").velocity("fadeIn");	
				$(".loading .icon").velocity("callout.pulse", {
					stagger: 300
				});
				setTimeout(function() {
					opponentsChoice();
					$(".choice-container label").removeClass("disabled");
					$("label[for=" + rps.opponentsChoice + "]").addClass("opponents-choice-active").velocity("callout.tada");
					$(".loading").velocity("fadeOut");
					$(".opponents-choice").html(rps.opponentsName + " chose "+ rps.opponentsChoice + "!");
					setTimeout(function() {
						determineResults();
						announceRoundResults();
						$(".instructions-container").velocity("transition.slideDownIn",{
							delay: 1000, 
							complete: function() {
								prepForNextRound();
							}	
						});
					}, 1500);
				}, 1500);
			}
		});

	}
}
function resetEverything() {
	//reset current game (choice, points) 
	rps.yourChoice = null;
	rps.opponentsChoice = null;
	rps.yourPoints = 0;
	rps.opponentsPoints = 0;
	rps.roundWinner = null;

	//reset ohCount
	ohCount = 0;

	//go straight to the opponents selection
	$(".the-winner").addClass("hide");
	$(".choose-opponent").removeClass("hide");

	//hide round score
	$(".score-card, .best-of").addClass("hide");
	$(".top-scores").removeClass("hide");

	//reset score display in header (head to head score)
	$(".score-card .your-score, .score-card .opponents-score").text("0");

	//disable buttons
	$(".confirm-opponent").addClass("disabled").attr("disabled", "disabled");


	//if you won, mark opponent as defeated
	if (rps.overallWinner === rps.yourName) {
		//mark the baddy as defeated (in DOM)
		$(".choose-opponent figure").eq(rps.opponentsId).addClass("defeated")
			.attr("disabled", "disabled")
			.find(".opponent-indication").html("&#10006;");
		//updated the baddies object 
		baddies[rps.opponentsId].defeated = true;

		//generally note in local storage that at least one baddy is defeated
		rpsStorage.setItem("defeatedEnemies", "true"); //general flag that enemies are defeated
		//update localStorage that a specific enemy is defeated
		rpsStorage.setItem("defeatedEnemy" + rps.opponentsId, "defeated" );
	}
	//opponent selection - remove active status from previous opponent
	$(".choose-opponent figure").eq(rps.opponentsId).removeClass("active");
}
function updateLeaderBoard() {
  ref.push({
      Name: rpsStorage.yourName,
      Score: Number(rpsStorage.yourScore)
    });
};
function rollCredits() {
	updateLeaderBoard();
	$(".final-boss").velocity("transition.slideDownOut", {
		complete: function() {
			$(".choose-opponent").velocity("transition.slideDownOut", {
				complete: function() {
					$.get("./partials/credits.html", function(credits) {
						$(".credits").html(credits).removeClass("hide");
						$(".credits .your-name-display").text(rps.yourName);
						//i want to show actual rolling credits that start from the top, and animate upwards. 
						//After the credits, all the baddies will be re-introduced
						//and then a final start over button
						//handlebars template
						var template = $("#credits-opponents-template").html();
						var compiledTemplate = Handlebars.compile(template); 
						$(".credits-baddies").html(compiledTemplate(badGuys));
						var creditsHeight = $(".credits-container").height();
						$(".credits-container").velocity(
						{ 
							translateZ: 0,
						 	translateY: -creditsHeight - 300
						},{
							duration: 120000,
							easing: "linear",
							complete: function() {
								location.assign('leader-board.html');
							}
						});
					});
				}
			});
		}
	});	
}
function introduceFinalBoss() {
	rps.opponentsId = 4;
	var $finalBoss = $(".opponent-grumpy-cat, .bio-id-4");
	$(".henchmen").velocity("transition.slideDownOut", {
		complete: function() {
			$("body").addClass("isolation-mode");
			$($finalBoss).insertAfter(".final-boss h1");
			$(".final-boss").addClass("show-boss").find(".opponent-container").removeClass("hide");
			$(".confirm-opponent.non-boss-opponents").addClass("hide");
		}
	});
	//TODO: this is a little clunkydunky
}
function howManyBaddiesDefeated() {
	var defeatedBaddies = 0;
	//check to see if all baddies are defeated...
	$.each(baddies, function(i, val) {
		if (val.defeated === true) {
			defeatedBaddies = defeatedBaddies + 1;
		} 
	});
	return defeatedBaddies;
}
function allBaddiesAreDefeated(number) {
	//if all baddies are defeated, promote to final boss level...
	if (number === baddies.length - 1) {
		setTimeout(function() {
			//rock, paper, scissors animation is shown over opponents selection that dissapears. 
			//User is taken to final boss page
			//final boss page shows boss bio, 
			introduceFinalBoss();

		}, 1500);
	} else if (number === baddies.length) {
		rollCredits();
	}
}
function playAgain() {
	resetEverything();
	//howManyBaddiesDefeated();
	allBaddiesAreDefeated(howManyBaddiesDefeated());

}
var $opponentBioId = null;
function openOpponentBio() {

	$opponentBioId = $("figure.active").data("opponent-id");
	$(".opponent-container").addClass("hide");
	$(".bio-id-" + $opponentBioId).removeClass("hide").addClass("is-presented");
	$(".confirm-opponent").addClass("hide");
	$("body").addClass("isolation-mode");
	if (rps.opponentsId === 4) {
		//$(".final-boss").removeClass("show-boss");
		$(".final-boss > h1, .final-boss .confirm-opponent").addClass("hide");
		$(".final-boss").addClass("bio-is-open");
	} 
	$("body").scrollTop(0);
}
function closeOpponentBio() {
	$(".bio-id-" + $opponentBioId).addClass("hide").removeClass("is-presented");
	$(".confirm-opponent").removeClass("hide");

	
	if (rps.opponentsId === 4) {
		$(".opponent-grumpy-cat").removeClass("hide");
		//$(".final-boss").addClass("show-boss");
		$(".final-boss > h1, .final-boss .confirm-opponent").removeClass("hide");

		$(".final-boss").removeClass("bio-is-open");
	} else {
		$("body").removeClass("isolation-mode");
		$(".opponent-container").not(".opponent-grumpy-cat").removeClass("hide");
	}
}

function forgetName(e) {
	e.preventDefault();
	rpsStorage.clear(); //clear everything saved in local storage
	rps.yourOverallScore = 0; //set your overall score back to 0
	rps.playTo = 1; //reset playTo back to 1

	//reset play
	$(".returning-player").addClass("hide");
	$(".new-player").removeClass("hide");
	$(".your-experience-container").addClass("hide");
	$(".rps-play-to").prop("checked", false);
	$(".rps-play-to[value=2]").prop("checked", true);
	//update button to show "Let's go", instead of "continute"
	$(".lets-go").text("Let's go!");
}

$(document).ready(function() {

	//fast click to eliminate 300ms delay on iOS devices
	FastClick.attach(document.body);

	//localstorage stuff
	if (rpsStorage.getItem("yourName")) {
		//show returning player stuff
		$(".new-player").addClass("hide");
		$(".returning-player").removeClass("hide");
		$(".your-name-display").text(rpsStorage.getItem("yourName"));
		//update button to show "Continue", instead of "Let's go"
		$(".lets-go").text("Continue!");
		
		//default the best of based on their last game
		if (rpsStorage.getItem("playTo")) {
			var val = Number(rpsStorage.getItem("playTo"));
			$(".rps-play-to").prop("checked", false);
			$(".rps-play-to[value=" + val + "]").prop("checked", true);
		}

		//if player has a score, show the experience points container
		if (rpsStorage.getItem("yourScore")) {
			rps.yourOverallScore = Number(rpsStorage.getItem("yourScore"));
			//display total experience points
			$(".your-experience-container .experience-points").text(rps.yourOverallScore);
			$(".your-experience-container").removeClass("hide");	
		}

	} else {
		//show new player stuff
		$(".new-player").removeClass("hide");
		$(".returning-player").addClass("hide");

	}
	$("body").on("click", ".lets-go", proceedToOpponentSelection);
	$("body").on("click", ".change-your-name", forgetName);
	$("body").on("click", ".choose-opponent figure", selectOpponent);
	$("body").on("click", ".open-opponent-bio", openOpponentBio);
	$("body").on("click", ".close-opponent-bio", closeOpponentBio);
	$("body").on("click", ".confirm-opponent", openOpponentBio);
	$("body").on("click", ".proceed-to-play", proceedToRPS);
	$("body").on("click", ".play-rps", playRPS);
	$("body").on("click", ".play-again", playAgain);
});